#include <stdio.h>
#include <pic16f887.h>
#include <stdlib.h>
#include <htc.h>

#define _XTAL_FREQ 8000000

typedef int (*get_distance_fct)();

int test_get_distance();
int get_distance_interrupt();

get_distance_fct setup_distance_finder_interrupt(int d_min, int d_max);
int get_distance_bussy_waiting();

get_distance_fct setup_distance_finder_bussy_waiting(int d_min, int d_max);