#include "distance_sensor.h"
#include <xc.h>

int a;
int d;
int D_MIN;
int D_MAX;

const int HIST_SIZE = 10;
int hist[10] = {0};

void add_to_hist(int v){
    int i;
    for(i=1;i<HIST_SIZE;i++){
        hist[i-1] = hist[i];
    }
    hist[HIST_SIZE-1] = v;
}

int last_val(){
    return hist[HIST_SIZE - 1];
}

int average(int v[], int v_size){
    int sum = 0;
    int i;
    for (i=0;i<v_size;i++){
        sum+=v[i];
    }
    return (int)(sum/v_size);
}

int normalized_distance(int d) {
    add_to_hist(d);
    return average(hist, HIST_SIZE);
}

void fill_hist(get_distance_fct get_distance){
    int i;
    for (i=0;i<HIST_SIZE;i++){
        (*get_distance)();
    }
}

get_distance_fct setup_distance_finder_bussy_waiting(int d_min, int d_max){
    
    TRISB = 0b00010000;         //RB4 as Input PIN (ECHO)
    T1CON = 0x10;               //Initialize Timer Module
    D_MIN = d_min;
    D_MAX = d_max;
    
    fill_hist(&get_distance_bussy_waiting);
    
    return &get_distance_bussy_waiting;
};

int get_distance_bussy_waiting(){
    TMR1H = 0;                //Sets the Initial Value of Timer
    TMR1L = 0;                //Sets the Initial Value of Timer

    RB0 = 1;                  //TRIGGER HIGH
    __delay_us(10);           //10uS Delay 
    RB0 = 0;                  //TRIGGER LOW

    while(!RB4);              //Waiting for Echo
    TMR1ON = 1;               //Timer Starts
    while(RB4);               //Waiting for Echo goes LOW
    TMR1ON = 0;               //Timer Stops

    a = (TMR1L | (TMR1H<<8)); //Reads Timer Value
    a = a/58.82;              //Converts Time to Distance
    a = a + 1;                //Distance Calibration
        
    if (a > D_MIN && a < D_MAX){
        return normalized_distance(a);
    } else {
        return 0;            // 0 is false in a boolean context 
    }                        // here 0 means error 
} 

void interrupt echo()
{
  if(RBIF == 1)                       //Makes sure that it is PORTB On-Change Interrupt
  {
    RBIE = 0;                         //Disable On-Change Interrupt
    if(RB4 == 1)                      //If ECHO is HIGH
    TMR1ON = 1;                       //Start Timer
    if(RB4 == 0)                      //If ECHO is LOW
    {
      TMR1ON = 0;                     //Stop Timer
      d = (TMR1L | (TMR1H<<8))/58.82; //Calculate Distance
    }
  }
  RBIF = 0;                           //Clear PORTB On-Change Interrupt flag
  RBIE = 1;                           //Enable PORTB On-Change Interrupt
}
get_distance_fct setup_distance_finder_interrupt(int d_min, int d_max) {
    // Setup code for the distance finder returns a function which returns
    // the distance in centimeters
    TRISB = 0b00010000;         //RB4 as Input PIN (ECHO)
    GIE = 1;                    //Global Interrupt Enable
    RBIF = 0;                   //Clear PORTB On-Change Interrupt Flag
    RBIE = 1;                   //Enable PORTB On-Change Interrupt
    __delay_ms(2000);
    T1CON = 0x10;               //Initialize Timer Module
    
    D_MIN = d_min;
    D_MAX = d_max;
    return &get_distance_interrupt;
};

int get_distance_interrupt(){
    TMR1H = 0;                //Sets the Initial Value of Timer
    TMR1L = 0;                //Sets the Initial Value of Timer

    RB0 = 1;                  //TRIGGER HIGH
    __delay_us(10);           //10uS Delay 
    RB0 = 0;                  //TRIGGER LOW

    __delay_ms(100);                  //Waiting for ECHO
    d = d + 1;                        //Error Correction Constant

    if(d>=2 && d<=100){               //Check whether the result is valid or not
        return d;
    }
    else {
        return 100;
    }
}

// The test function for getting a distance in centimeters from 1 to 400
int TEST_DISTANCES[] = {200, 400 ,2, 99, 100, 101, 150, 199, 200, 201, 299, 300, 301, 399, 400, 400, 400};
int TEST_POSITION = 0;
int test_get_distance(){
    int ret = TEST_DISTANCES[TEST_POSITION];
    if (TEST_POSITION == 15) {
        TEST_POSITION = 0;
    } else {
        TEST_POSITION++;
    }
    return ret;
}
