#include <stdio.h>
#include <pic16f887.h>
#include <stdlib.h>
#include <htc.h>

#include "sensor/distance_sensor.h"
#include "actuators/actuators.h"

#define countof(a) (sizeof(a) / sizeof(*(a)))   // get the size of any array
// BEGIN CONFIG
#pragma config FOSC = HS   
#pragma config WDTE = OFF  
#pragma config PWRTE = OFF 
#pragma config BOREN = ON 
#pragma config LVP = OFF   
#pragma config CPD = OFF   
#pragma config WRT = OFF  
#pragma config CP = OFF    
//END CONFIG
// program loop
void start_app(get_distance_fct get_distance, 
               notify_actuator_fct actuators[], 
               int actuator_count){
    //Gets the distance and updates all actuators with the new value
    
     while(1){
        int d = (*get_distance)();
        if (d){
            notify_actuators(d, actuators, actuator_count);
        }
        __delay_ms(10);
     }
}

int main(int argc, char** argv) {
    
    int D_MIN = 2;
    int D_MAX = 400;
    int N_LEDS = 8;
    
    notify_actuator_fct actuators[2];
    
    // specify the min and max distance that the distance sensor can sense in cm
    // and the number of leds on the led board
    actuators[0] = setup_led_actuator(D_MIN, D_MAX, N_LEDS);
    actuators[1] = setup_buzzer_actuator(D_MIN, D_MAX);
    
    start_app( 
        setup_distance_finder_bussy_waiting(D_MIN, D_MAX),
//        setup_distance_finder_interrupt(D_MIN, D_MAX),
//        &test_get_distance,   // using the test data until the sensor is installed
 
        actuators, countof(actuators)
    );
    
    return (EXIT_SUCCESS);
}
