#include "actuators.h"

void notify_actuators(int distance, notify_actuator_fct actuators[], int actuator_count) {
    int i = 0;
    for (i = 0; i< actuator_count; i++){
        (*actuators[i])(distance);
    }
}