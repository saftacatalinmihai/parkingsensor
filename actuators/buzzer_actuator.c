#include "actuators.h"
#include <pic16f887.h>
#include <stdlib.h>
#include <htc.h>

#define _XTAL_FREQ 8000000

int D_MIN = 0;
int D_MAX = 0;

void buzz(int duration){
    // duration in ms
    // The frequency is set to ~ 880 Hz ( 1 / 1100 )
    int i = 0;
    for (i=0;i<duration/8.8;i++){
        RC0 = 1;
        __delay_us(1100);
        RC0 = 0;
        __delay_us(1100);
    }
}

int d_to_buzz_interval(int d){
    return _map_interval(d, D_MIN, D_MAX, 50);
}

int ticks_since_last_buzz = 0;


notify_actuator_fct setup_buzzer_actuator(int d_min, int d_max){
    TRISC=0x00;     // Set Port C as output.
    PORTC=0x04;
    ticks_since_last_buzz = 0;
    D_MIN = d_min;
    D_MAX = d_max;
    return &notify_buzzer_actuator;
}

void notify_buzzer_actuator(int d){
//    printf("Buzzer buzzes: %d\n", d);
    
    if (d < D_MAX) {
        if (ticks_since_last_buzz > d_to_buzz_interval(d)) {
            buzz(100);
            ticks_since_last_buzz = 0;
        } else {
            ticks_since_last_buzz += 1;
        }
    } else {
        ticks_since_last_buzz = 0;
    }
    
    
}