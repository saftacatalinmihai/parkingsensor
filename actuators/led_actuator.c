#include "actuators.h"
#include <math.h>

int _map_negative_exp(int d, int d_min, int d_max, int range) {
    return (int)(floor((range+1) * exp(-(double)(d-d_min)/((d_max-d_min)/4))));
}


int _to_hex(int n_leds){
    // (2^n_leds) - 1
    return n_leds == 0 ? 0x00 : (1<<n_leds) - 1;  
}

int D_MIN = 0;
int D_MAX = 0;
int NUM_OF_LEDS = 0;
notify_actuator_fct setup_led_actuator(int d_min, int d_max, int num_of_leds){
    ANSELH=0x00;
    ANSEL=0x00;
    TRISD=0x00;
    
    D_MIN = d_min;
    D_MAX = d_max;
    NUM_OF_LEDS = num_of_leds;
    return &notify_led_actuator;
}

void notify_led_actuator(int d){
    // The distance is in centimeters
    // The min distance is D_MIN ( 2 cm for the HC - SR04 sensor)
    // The max distance is D_MAX ( 400 cm for the HC - SR04 sensor ) 
    // The d is guaranteed to be between in that interval from the invariant postcondition 
    // of the distance getter function
    
    // The formula is to map the distance to an interval from 0 to 8
    // take a negative exponential function which maps from the distance to an integer 
    // and map that to hex to light the appropriate amount of leds.
    
    // There are 8 leds on the led bar on the pic demo board
    PORTD = _to_hex(_map_negative_exp(d, D_MIN, D_MAX, NUM_OF_LEDS));
}
