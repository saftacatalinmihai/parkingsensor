#include <stdio.h>
#include <pic16f887.h>
#include <stdlib.h>
#include <htc.h>

typedef void (*notify_actuator_fct)(int d);

static int _map_interval(int d, int d_min, int d_max, int range) {
    return (d - d_min) * range / (d_max - d_min);
}

notify_actuator_fct setup_buzzer_actuator(int d_min, int d_max);
void notify_buzzer_actuator(int d);

notify_actuator_fct setup_led_actuator(int d_min, int d_max, int num_of_leds);
void notify_led_actuator(int d);

void notify_actuators(int distance, notify_actuator_fct actuators[], int actuator_count);